<?php include_once('config/mainx.php');
$query=mysqli_query($koneksi,"select * from soal where idSoal=".$_GET['id']);
$data = mysqli_fetch_array($query);
$idku = $_GET['id'];
?>

<?php
  if(!isset($_SESSION['username'])){ 
    header("Location: ../../login.php");
}
?>
<section>
	<div class="row">
		<div class="col-md-12">
	      <!-- general form elements disabled -->
	      <div class="box box-warning">
	        <div class="box-header">
	          <h3 class="box-title">Tambah Soal</h3>
	        </div><!-- /.box-header -->
	        <div class="box-body">
	          <form role="form" method="post" action="simpan.php?type=detailSoal&kategori=<?php echo $_GET['kategori'] ?>&idKategori=<?php echo $_GET['idKategori'] ?>">
	          <input type="hidden" name="type" value="detailSoal">
	           <input type="hidden" name="cmd" value="edit">
			   <input type="hidden" name="id" value="<?php echo $idku ?>">
	            <!-- text input -->
				<div class="form-group">
	              <label>Soal</label>
	              <textarea class="form-control" name="soal" rows="3" placeholder="masukkan soal"><?php echo $data['soal']; ?></textarea>
	            </div>
	            <div class="form-group">
	              <label>Option A</label>
	              <input type="text" name="opsiA" class="form-control" placeholder="opsi a" value="<?php echo $data['opsiA']; ?>"/>
	            </div>
				<div class="form-group">
	              <label>Option B</label>
	              <input type="text" name="opsiB" class="form-control" placeholder="opsi b" value="<?php echo $data['opsiB']; ?>"/>
	            </div>
				<div class="form-group">
	              <label>Option C</label>
	              <input type="text" name="opsiC" class="form-control" placeholder="opsi c" value="<?php echo $data['opsiC']; ?>"/>
	            </div>
				<div class="form-group">
	              <label>Option D</label>
	              <input type="text" name="opsiD" class="form-control" placeholder="opsi d" value="<?php echo $data['opsiD']; ?>"/>
	            </div>
				<div class="form-group">
	              <label>Jawaban</label>
	              <select name="jawaban">
	              	<option value="a" <?php if($data['jawaban'] == 'a') { echo 'selected'; } ?>>A</option>
					<option value="b" <?php if($data['jawaban'] == 'b') { echo 'selected'; } ?> >B</option>
					<option value="c" <?php if($data['jawaban'] == 'c') { echo 'selected'; } ?>>C</option>
					<option value="d" <?php if($data['jawaban'] == 'd') { echo 'selected'; } ?>>D</option>
	              </select>
	            </div>
	            <button type="submit" class="btn btn-success"> <i class="fa fa-save"></i> Simpan</button>
	            <button type="reset" class="btn btn-warning"> <i class="fa fa-trash"></i> Reset</button>
	            <a href="index.php?page=detailSoal&id=<?php echo $_GET['idKategori'] ?>&kategori=<?php echo $_GET['kategori'] ?>" class="btn btn-danger"> <i class="fa fa-times"></i> Batal</a>
	          </form>
	        </div><!-- /.box-body -->
	      </div><!-- /.box -->
	    </div><!--/.col (right) -->
	</div>
</section>