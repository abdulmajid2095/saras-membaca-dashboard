<?php include_once('config/mainx.php');
$query=mysqli_query($koneksi,"select * from kategori where idKategori=".$_GET['id']);
$data = mysqli_fetch_array($query);
?>

<?php
  if(!isset($_SESSION['username'])){ 
    header("Location: ../../login.php");
}
?>

<section>
	<div class="row">
		<div class="col-md-12">
	      <!-- general form elements disabled -->
	      <div class="box box-warning">
	        <div class="box-header">
	          <h3 class="box-title">Edit Pengguna</h3>
	        </div><!-- /.box-header -->
	        <div class="box-body">
	          <form role="form" method="post" action="simpan.php" enctype="multipart/form-data">
	          <input type="hidden" name="type" value="kategori">
	           <input type="hidden" name="cmd" value="edit">
	          <input type="hidden" name="id" value="<?php echo $data['idKategori']; ?>">
			  <input type="hidden" name="namaFile" value="<?php echo $data['gambar']; ?>">
	            <!-- text input -->
				<div class="form-group">
	              <label>Judul</label>
	              <input type="text" name="judul" class="form-control" placeholder="judul" value="<?php echo $data['judul']; ?>"/>
	            </div>
				<div class="form-group">
	              <label>Deskripsi</label>
	              <textarea class="form-control" name="deskripsi" rows="3" placeholder="deskripsi" ><?php echo $data['deskripsi']; ?></textarea>
	            </div>
				<div class="form-group">
	              <label>Bacaan</label>
	              <textarea class="form-control" name="bacaan" rows="10" placeholder="bacaan"><?php echo $data['bacaan']; ?></textarea>
	            </div>
				<div class="form-group">
	              <label>Jumlah Kata</label>
	              <input type="text" name="kata" class="form-control" placeholder="judul" value="<?php echo $data['jumlahKata']; ?>"/>
	            </div>
				<img src="../image/<?php echo $data['gambar']?>" width="100" height="100"/>

				<div class="form-group">
	              <label>Ganti Gambar (Abaikan jika tidak ingin diganti)</label>
	              <input type="file" name="berkas"  class="btn btn-success">
	            </div>

	            <button type="submit" class="btn btn-success"> <i class="fa fa-save"></i> Simpan</button>
	            <button type="reset" class="btn btn-warning"> <i class="fa fa-backward"></i> Kembalikan Data </button>
	            <a href="index.php?page=admin" class="btn btn-danger"> <i class="fa fa-times"></i> Batal</a>
	          </form>
	        </div><!-- /.box-body -->
	      </div><!-- /.box -->
	    </div><!--/.col (right) -->
	</div>
</section>