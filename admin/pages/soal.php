<?php include_once('config/mainx.php');
$query=mysqli_query($koneksi, "select * from kategori");
?>
<?php
  if(!isset($_SESSION['username'])){ 
    header("Location: ../login.php");
}
?>

<div class="row">
<?php
		  $no=1;
		  while($q=mysqli_fetch_array($query)){
?>

<?php
$id = $q['idKategori'];
$data_soal=mysqli_query($koneksi,"select * from soal where soal.idKategori = $id");
 ?>
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
          <div class="inner">
            <h3><?php echo mysqli_num_rows($data_soal); ?> Soal</h3>
            <p><?php echo $q['judul']; ?></p>
          </div>
          <div class="icon">
            <i class="fa fa-book"></i>
          </div>
          <a href="./?page=detailSoal&id=<?php echo $id ?>&kategori=<?php echo $q['judul']; ?>" class="small-box-footer">Lihat Soal <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div><!-- ./col -->
  
<?php
	}
?>

</div><!-- /.row -->
    <script src="plugins/jQuery/jQuery-2.1.3.min.js"></script>