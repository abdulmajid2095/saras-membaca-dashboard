<?php include_once('config/mainx.php');
$id = $_GET['id'];
$query=mysqli_query($koneksi, "select * from soal where idKategori = $id");
?>
<?php
  if(!isset($_SESSION['username'])){ 
    header("Location: ../login.php");
}
?>
<div class="box">
    <div class="box-header">
      <h3><?php echo $_GET['kategori']; ?> </h3>
	  <h5>( Terdapat <?php echo mysqli_num_rows($query); ?> Soal )</h5>
    </div><!-- /.box-header -->
    <div class="box-body">
    <?php if (isset($_SESSION['username'])): ?>
     <a href="tambah.php?tambah=soal&idKategori=<?php echo $_GET['id']; ?>&kategori=<?php echo $_GET['kategori']; ?>" style="margin-bottom: 10px;" class="btn btn-md btn-primary"> <i class="fa fa-plus"></i> Tambah Soal</a>
 <?php endif; ?>
		<table class="table table-bordered" id="tabel">
		<thead>
			<tr>
		    <th>NO</th>
		    <th>SOAL</th>
		    <th>OPSI A</th>
		    <th>OPSI B</th>
			<th>OPSI C</th>
			<th>OPSI D</th>
			<th>JAWABAN</th>
		    <?php if (isset($_SESSION['username'])): ?>
		    <th></th>
			<?php endif; ?>
		  </tr>
		</thead>
		<tbody>
			<?php
		  $no=1;
		  while($q=mysqli_fetch_array($query)){
		  ?>
		  <tr>
		    <td><?php echo $no++; ?></td>          
		    <td><?php echo $q['soal']?></td>
		    <td><?php echo $q['opsiA']?></td>
		    <td><?php echo $q['opsiB']?></td>
			<td><?php echo $q['opsiC']?></td>
			<td><?php echo $q['opsiD']?></td>
			<td><?php echo $q['jawaban']?></td>
		    <?php if (isset($_SESSION['username'])): ?>
		    <td>
		    	<a class="btn btn-success" href="edit.php?edit=soal&id=<?php echo $q['idSoal']; ?>&idKategori=<?php echo $_GET['id']; ?>&kategori=<?php echo $_GET['kategori']; ?>">Edit</a>
		    	<a class="btn btn-danger" onclick="return confirm('Apakah anda yakin akan menghapus data ini?')" href="hapus.php?hapus=<?php echo $_GET['page']; ?>&id=<?php echo $q['idSoal']; ?>&idKategori=<?php echo $_GET['id']; ?>&kategori=<?php echo $_GET['kategori']; ?>">Hapus</a>
		    </td>
			<?php endif; ?>
		  </tr>
		  <?php
		  }
		  ?>
		</tbody>
		  
		</table>
	</div>
</div>
<script src="plugins/jQuery/jQuery-2.1.3.min.js"></script>
<script src="plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
 <script type="text/javascript">
	 $(document).ready(function() {
	 	$('#tabel').dataTable({
	          "bPaginate": true,
	          "bLengthChange": true,
	          "bFilter": true,
	          "bSort": true,
	          "bInfo": true,
	          "bAutoWidth": true
	    });
	 });
</script>