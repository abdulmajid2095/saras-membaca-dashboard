
<?php
  if(!isset($_SESSION['username'])){ 
    header("Location: ../../login.php");
}
?>
<section>
	<div class="row">
		<div class="col-md-12">
	      <!-- general form elements disabled -->
	      <div class="box box-warning">
	        <div class="box-header">
	          <h3 class="box-title">Tambah Kategori</h3>
	        </div><!-- /.box-header -->
	        <div class="box-body">
	          <form role="form" method="post" action="simpan.php" enctype="multipart/form-data">
	          <input type="hidden" name="type" value="kategori">
	           <input type="hidden" name="cmd" value="tambah">
	            <!-- text input -->
	            <div class="form-group">
	              <label>Judul</label>
	              <input type="text" name="judul" class="form-control" placeholder="judul" value=""/>
	            </div>
				<div class="form-group">
	              <label>Deskripsi</label>
	              <textarea class="form-control" name="deskripsi" rows="3" placeholder="deskripsi"></textarea>
	            </div>
				<div class="form-group">
	              <label>Bacaan</label>
	              <textarea class="form-control" name="bacaan" rows="10" placeholder="bacaan"></textarea>
	            </div>
				<div class="form-group">
	              <label>Jumlah Kata</label>
	              <input type="text" name="kata" class="form-control" placeholder="jumlah kata" value=""/>
	            </div>
				<div class="form-group">
	              <label>Pilih file untuk upload</label>
	              <input type="file" name="berkas"  class="btn btn-success">
	            </div>
				
	            <button type="submit" class="btn btn-success"> <i class="fa fa-save"></i> Simpan</button>
	            <button type="reset" class="btn btn-warning"> <i class="fa fa-trash"></i> Reset</button>
	            <a href="index.php?page=kategori" class="btn btn-danger"> <i class="fa fa-times"></i> Batal</a>
	          </form>
	        </div><!-- /.box-body -->
	      </div><!-- /.box -->
	    </div><!--/.col (right) -->
	</div>
</section>