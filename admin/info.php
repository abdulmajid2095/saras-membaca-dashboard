<?php include_once('config/mainx.php');
$data_kategori = mysqli_query($koneksi,"select * from kategori");
$data_soal=mysqli_query($koneksi,"select * from soal");
$data_skor=mysqli_query($koneksi,"select * from skor");
 ?>

<?php
  if(!isset($_SESSION['username'])){ 
    header("Location: login.php");
}
?>
<div class="row">
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <h3><?php echo mysqli_num_rows($data_kategori); ?></h3>
          <p>Data Kategori</p>
        </div>
        <div class="icon">
          <i class="fa fa-newspaper-o"></i>
        </div>
        <a href="./?page=kategori" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3><?php echo mysqli_num_rows($data_soal); ?></h3>
          <p>Data Soal</p>
        </div>
        <div class="icon">
          <i class="fa fa-book"></i>
        </div>
        <a href="./?page=soal" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-red">
        <div class="inner">
          <h3><?php echo mysqli_num_rows($data_skor); ?></h3>
          <p>Data Skor</p>
        </div>
        <div class="icon">
          <i class="fa fa-trophy"></i>
        </div>
        <a href="./?page=skor" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- ./col -->
  </div><!-- /.row -->
  <script src="plugins/jQuery/jQuery-2.1.3.min.js"></script>