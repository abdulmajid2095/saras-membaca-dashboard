<?php
header('Content-type: application/json');
include_once('koneksi.php');


if( isset($_POST['idKategori']) )
{
  $idKategori = $_POST['idKategori'];

  $query = "SELECT * FROM soal WHERE soal.idKategori LIKE $idKategori";
  $result = mysqli_query($koneksi,$query);
  $array_data = array();
  while($baris = mysqli_fetch_assoc($result))
  {
    $array_data[]=$baris;
  }

  echo json_encode($array_data);        

}
else
{
    $response['code'] = 500;
    $response['status'] = "failed";
    $response['message'] = "id Kategori tidak valid";
    echo json_encode($response);
}


?>